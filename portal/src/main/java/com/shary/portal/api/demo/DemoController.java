package com.shary.portal.api.demo;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
class DemoController {
	
	private final DemoService demoService;
	
	@GetMapping("/demo")
	List<DemoEntity> getAllDemos() {
		return demoService.getAllDemos();
	}
}
