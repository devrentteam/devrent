package com.shary.portal.api.registration;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.shary.portal.security.user_details.CustomUserDetails;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
class RegistrationController {
	
	private final RegistrationService registrationService;
	
	@PostMapping("/registration")
	public String register(@RequestBody RegistrationDto registrationDto) {
		
		this.registrationService.saveUser(registrationDto);
		
		return "success";
	}
}
