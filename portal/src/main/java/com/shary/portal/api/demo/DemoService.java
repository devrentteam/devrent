package com.shary.portal.api.demo;

import java.util.List;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DemoService {
	
	private final DemoRepository demoRepository;
	
	List<DemoEntity> getAllDemos() {
		return demoRepository.findAll();
	}
}
