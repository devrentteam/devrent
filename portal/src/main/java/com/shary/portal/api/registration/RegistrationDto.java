package com.shary.portal.api.registration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RegistrationDto {
	private String username;
	private String name;
	private String surname;
	private String password;
	private String email;
}
