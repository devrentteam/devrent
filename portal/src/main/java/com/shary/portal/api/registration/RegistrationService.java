package com.shary.portal.api.registration;

import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.shary.portal.security.user_details.CustomUserDetails;
import com.shary.portal.security.user_details.CustomUserDetailsService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RegistrationService {
	
	private final ModelMapper modelMapper;
	
	private final BCryptPasswordEncoder passwordEncoder;
	
	private final CustomUserDetailsService customUserDetailsService;
	
	public void saveUser(RegistrationDto registrationDto) {
		final CustomUserDetails userDetails =  convertToEntity(registrationDto);
		userDetails.setPassword(passwordEncoder.encode(userDetails.getPassword()));
		this.customUserDetailsService.saveUserDetails(userDetails);
	}
	
	private CustomUserDetails convertToEntity(RegistrationDto registrationDto) {
		CustomUserDetails userDetails = modelMapper.map(registrationDto, CustomUserDetails.class);
	    return userDetails;
	}
}
