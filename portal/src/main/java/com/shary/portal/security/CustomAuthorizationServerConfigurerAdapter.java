package com.shary.portal.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import com.shary.portal.security.user_details.CustomUserDetailsService;


@Configuration
@EnableAuthorizationServer
public class CustomAuthorizationServerConfigurerAdapter extends AuthorizationServerConfigurerAdapter {

	private static final String CLIENT_ID = "shary";
	private static final String CLIENT_SECRET = "123456";
	private static final String SCOPE = "server";
	private static final String GRANT_TYPE_PASSWORD = "password";
	private static final String GRANT_TYPE_REFRESH = "refresh_token";
	private static final int ACCESS_TOKEN_VALIDITY_SECONDS = 2 * 60 * 60;
	private static final int REFRESH_TOKEN_VALIDITY_SECONDS = 2 * 60 * 60;

	private static final String publicKey = "-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxEZalZcd72M8w3yKusnc\r\n" + 
			"bdHzLYR/U8Q9b1IB/o4GjB7yTQaje1/UvGmRlJrQhbEOt3QNsJW8w2yQINpn4lza\r\n" + 
			"rX9rKMC8V0uyuBpOSmroqJwjFbgaKlsdQoGKVtQKdbG1s+YCHBasOVlp3rpo6CHk\r\n" + 
			"tuATtl28auMNp8jv9jJ594CY5G4YLqrfXZVGfQzYtlWK3rE1Eqdu6HSZtAZjtvdX\r\n" + 
			"g0PdFMJTwgDQWhPzx6kf7hKb6nOb2y6f9CZRzLkZj4KIg4ac1j7zj0LImRC3oLSK\r\n" + 
			"9cSklM53otiOJ0AyKxxX87gpzLys/3OC2i0KKJ9lQp2j/7rpgLontFHfabbtskCp\r\n" + 
			"hwIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";
	
	private static final String privateKey = "-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEpAIBAAKCAQEAtqTUAV2PZdy6u6xdTAgoxeyl4o61IOx60fpsSiul9OCWXaey\r\n" + 
			"OrqHLxScDAONADa225JJxsjZbbZe1FSkAxr0NypKnDQ6MRofCxfDCCr5YkRlwIGC\r\n" + 
			"uwM9211gz0CzA5N+mY5/VuEiQ310Qf8MTOv+WOqa8axs8fxiqwTG85znEm2Yk+7G\r\n" + 
			"BNVBKLbpZdrjKtyYM5tlArg9+vbajCh/WJmZNPU7jht3sbAz16C9wnpq2RZe/0Uc\r\n" + 
			"yvSYrBjZB5U1W1ROrk0nwriuTEdF9J1UFjBeqo1d9mhQrdW0z874sHO+/vS1ioXO\r\n" + 
			"rB1cpjqtyFoQt360le4HbsgymWjM3406MubFMwIDAQABAoIBADC29fUHR2Ab55J2\r\n" + 
			"/stwwa9I11X+SkFl0bx/1rJRwgn2fSStvdP8PGJKVPS/XbRY45zc/+CNYbDpm5F2\r\n" + 
			"oehSICE2nn/bTkoadX038UUUcT/MjncyE/RdgPophdDBqP2jMXkzkLNTiRgHNiME\r\n" + 
			"K/u4VjbRHFKpcsV4U7jZpVcE2n0HWroj2V3+NIo0wQ3ao3HLp4UZ55rVzusLKqDy\r\n" + 
			"mhfhtqkL5zuVdqf1xz5OCdWQhl/KqjnBy7497jAu44xKFv1WWwkd/FdREzxW3h75\r\n" + 
			"rppQ4rKRnZs91+e0nZUjk3cdP74z0Jq2avbxOVMxpM9RsQiSKdVaMd13IBVYcA39\r\n" + 
			"EdAH88ECgYEA5t6UtvvosD8NADy9N3bCfUNlq6hpmoAxC+zOZ1S3x63gpd64HjpX\r\n" + 
			"UzLdTWHDgJeVY2odYpS6u0egySfu3lUvZDpFZNBaQA9bzxijf2bXJZn7fd/WjFDm\r\n" + 
			"fl+Fn0aUAvU+PUSYAYJ4jHhV5e8aLtnwdRoG3reNpVvJvIEhUI1tFXUCgYEAyoZj\r\n" + 
			"91dP4fuwEpkRs30Mfr/rBcpaNkWM5naetNv9uXgLyx5jqwyMFtR9BbUJdPi7ENdp\r\n" + 
			"BgPwVsznT8w8QodpzUmaQHee1ST84q4rExLxse0BpVGRmpJpVe38IV7ZqKfUextL\r\n" + 
			"zLuRIEDymxQWSNFw7BJH1F0dxAHcYMPtJVMVkwcCgYEAgg1kjOe/HnmGI1oT5Qib\r\n" + 
			"auDqhxZp1sTb3DHb282XcX57jDS4Vx7aTl3a2Fp4PRom50fBoeTSg+lHo8lBADHQ\r\n" + 
			"bo98w9uy219LgpAImh4emGnzh+bt/0EbdpjsttqVSk1IcE8RqdgLfGXVTZ+xii7v\r\n" + 
			"C6hLsm6H9lyMegnr8GqaxnkCgYEAswLnCZGbglw93bJ++u/lQ4PP8WZTJvWC/ZKL\r\n" + 
			"5cEuG+oIQpmx71c/3E4l1A2ZNlNExjYhFrgUkCS9sMMN4KBg3JvaRHF3a18uSA86\r\n" + 
			"87ki+epMMhxPXxlbEiExYy27pl/yaH5QGb8RIDlIN9shd2vpLEFcTFGiJcafTMjh\r\n" + 
			"rMQrpbECgYBBvxZYi/nclwXnNI7HaKKSdG+VlV/m7M0buvJrABzmeDhxWWdtj481\r\n" + 
			"IqL4xFrxfAKsnGvQyF6S1BUY6BSTHEO+gkRhZTF34Cjw4I9CgKXMX5L50NglH+7Z\r\n" + 
			"7LXIGEoqkB1FVQBzaU2z5+J5fKOVPm+SrXwvQhCy055TyC1MxXbo4w==\r\n" + 
			"-----END RSA PRIVATE KEY-----";

	
	
	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("encoder")
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private DataSource datasource;
	
	@Autowired
	private TokenStore tokenStore;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;


	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints
				.tokenStore(tokenStore)
				//.accessTokenConverter(tokenEnhancer())
				.authenticationManager(authenticationManager)
				.userDetailsService(customUserDetailsService)
				.allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {

		configurer.inMemory()
				.withClient(CLIENT_ID)
				.secret(bCryptPasswordEncoder.encode(CLIENT_SECRET))
				.authorizedGrantTypes(GRANT_TYPE_REFRESH,GRANT_TYPE_PASSWORD)
				.scopes(SCOPE)
				.accessTokenValiditySeconds(ACCESS_TOKEN_VALIDITY_SECONDS)
				.refreshTokenValiditySeconds(REFRESH_TOKEN_VALIDITY_SECONDS);
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}

    @Bean
    public OAuth2AccessDeniedHandler oauthAccessDeniedHandler() {
        return new OAuth2AccessDeniedHandler();
    }
	
    @Bean
    public FilterRegistrationBean corsFilter()
    {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

	@Bean
	public TokenStore tokenStore() {
		return new JdbcTokenStore(datasource);
		//return new JwtTokenStore(tokenEnhancer());
	}
	
//	@Bean
//	public JwtAccessTokenConverter tokenEnhancer() {
//		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//		converter.setSigningKey(privateKey);
//		converter.setVerifierKey(publicKey);
//		return converter;
//	}

}
