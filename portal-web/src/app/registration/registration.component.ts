import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../services/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private registrationService: RegistrationService) { }

  ngOnInit() {
  }

  signUp(formValue) {
    console.log(JSON.stringify(formValue));
    this.registrationService.signUp(formValue).subscribe( 
      adsa => {
        console.log('Kayıt başarılı');
      }
    )
  }
}
