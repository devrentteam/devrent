import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  private url = 'http://localhost:8080/registration'

  constructor(private http:HttpClient) { }

  signUp(registrationDto) {

    return this.http.post(this.url, registrationDto);
  }
}
